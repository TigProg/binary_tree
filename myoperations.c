//------------------if press 0 - exit and garbage collection-------------------
void exit_prod(struct tree * prod_tree)
{
	printf("The tree is currently being deleted\n");

	clock_t t;
	t = clock();
	bin_destroy(prod_tree);
	t = clock() - t;

	printf("Tree deleted\n");
	printf("The operating time of the function is %f\n", ((float)t)/CLOCKS_PER_SEC);
}


//----------------if press 1 - insert a Node by key with data------------------
void insert_prod(struct tree * prod_tree)
{
	int key = NULL;
	int res;
	char s[255];
	char garbage[255];
	printf("Enter the key and press <ENTER>:\n");
	while(1)
	{
		scanf("%d",&key);
		scanf("%*[^\n]");
		if (key == NULL) printf("please try again\n");
		else break;
	}
	fgets(garbage, 255, stdin);
	printf("Enter the data (string) and press <ENTER>:\n");
	fgets(s, 255, stdin);

	clock_t t;
	t = clock();	
	res = insert(prod_tree, s, key);
	t = clock() - t;

	if (res == 0) 
	{
		printf("\nNode successfully inserted\n");
	}
	else if (res == 1)
	{
		printf("\nSorry, but a node with such a key already exists\n");
	}
	else if (res == 9)
	{
		printf("\nERROR IN INSERT BY KEY\n");
	}

	printf("The operating time of the function is %f\n\n", ((float)t)/CLOCKS_PER_SEC);
}


//----------------------if press 2 - delete a Node by key----------------------
void delete_prod(struct tree * prod_tree)
{
	int key = NULL;
	int res;
	printf("Enter the key and press <ENTER>:\n");
	while(1)
	{
		scanf("%d",&key);
		scanf("%*[^\n]");
		if (key == NULL) printf("please try again\n");
		else break;
	}

	clock_t t;
	t = clock();	
	res = delete(prod_tree, key);
	t = clock() - t;

	if (res == 0) 
	{
		printf("\nNode successfully deleted\n");
	}
	else if (res == 1)
	{
		printf("\nSorry, but a node with such a key already exists\n");
	}
	printf("The operating time of the function is %f\n\n", ((float)t)/CLOCKS_PER_SEC);
}


//----------------------if press 3 - search a Node by key----------------------
void search_prod(struct tree * prod_tree)
{
	int key = NULL;
	int res;
	printf("Enter the key and press <ENTER>:\n");
	while(1)
	{
		scanf("%d",&key);
		scanf("%*[^\n]");
		if (key == NULL) printf("please try again\n");
		else break;
	}
	struct node * target_node;

	clock_t t;
	t = clock();
	target_node = bin_search(prod_tree, key);
	t = clock() - t;

	if (target_node == NULL)
	{
		printf("\nNode with key = %d not found\n",key);
	}
	else
	{
		printf("\nNode with key = %d found:\n",key);
		print_node(target_node);	
	}
	
	printf("The operating time of the function is %f\n\n", ((float)t)/CLOCKS_PER_SEC);
}


//---------------if press 4 - print tree like table using *next----------------
void print_matrix_prod(struct tree * prod_tree)
{
	clock_t t;
	t = clock();
	print_matrix(prod_tree);
	t = clock() - t;
	printf("The operating time of the function is %f\n\n", ((float)t)/CLOCKS_PER_SEC);
}


//----------------5 - search Node with next key in direct order----------------
void print_next_node_prod(struct tree * prod_tree)
{
	int key = NULL;
	int res;
	printf("Enter the key and press <ENTER>:\n");
	while(1)
	{
		scanf("%d",&key);
		scanf("%*[^\n]");
		if (key == NULL) printf("please try again\n");
		else break;
	}
	struct node * target_node;

	clock_t t;
	t = clock();
	target_node = next_node_f(prod_tree, key);
	t = clock() - t;

	if (target_node == NULL)
	{
		printf("\nNode with key > %d not found\n",key);
	}
	else
	{
		printf("\nNode with key > %d found:\n",key);
		print_node(target_node);	
	}
	printf("The operating time of the function is %f\n\n", ((float)t)/CLOCKS_PER_SEC);
}


//--------------if press 6 - print tree like tree using recursion--------------
void print_tree_prod(struct tree * prod_tree)
{
	clock_t t;
	t = clock();
	walk2(prod_tree);
	t = clock() - t;
	printf("The operating time of the function is %f\n\n", ((float)t)/CLOCKS_PER_SEC);
}


//---------------------if press 7 - upload tree from file----------------------
void upload_tree_prod(struct tree * prod_tree)
{
//not tested
	printf("Read from 'example_tree.txt'\n");
	FILE *myfile;
	myfile = fopen("example_tree.txt","r");
	clock_t t;
	t = clock();
	if (myfile == NULL) { printf("File not found\n"); }
	else
	{
		int n;
		int i = 0;
		int res;
		int key;
		char s[255], garbage[255];
		int check = 0;
		fscanf(myfile,"%d",&n);
		while(i<n)
		{
			fscanf(myfile,"%d",&key);
			fgets(garbage, 255, myfile);
			fgets(s, 255, myfile);
			res = insert(prod_tree, s, key);
			if (res == 0) 
			{
				printf("%d node successfully inserted from file\n",i+1);
				printf("%d\t-\t",key);
				fputs(s,stdout);
			}
			else if (res == 1)
			{
				printf("\nSorry, but a node with such a key already exists\n");
			}
			else if (res == 9)
			{
				printf("\nERROR IN INSERT BY KEY\n");
				printf("CRASH PROGRAM\n");
				check = 1;
				break;
			}
			i += 1;
		}
		fclose(myfile);
		if (check == 1)
		{
			printf("The tree is currently being deleted\n");
			bin_destroy(prod_tree);
			printf("Tree deleted\n");	
		}
		else
		{
			printf("File successfully uploaded\n");
		}
	}
	t = clock() - t;
	printf("The operating time of the function is %f\n\n", ((float)t)/CLOCKS_PER_SEC);
}


//main function in this program
//interlayer between 'frontend' in main.c and 
//					 'backend' in mystruct.c
int agregator_prod(int k, struct tree * my_tree_agr){
	if ((k<=7)&&(k>=0))
	{
		printf("you press %d\n\n",k);
		switch (k) {
			case 0:
				exit_prod(my_tree_agr);
				return (-1);       
				break;
			case 1:
				insert_prod(my_tree_agr);
				break;
			case 2:
				delete_prod(my_tree_agr);
				break;
			case 3:
				search_prod(my_tree_agr);
				break;
			case 4:
				print_matrix_prod(my_tree_agr);
				break;
			case 5:
				print_next_node_prod(my_tree_agr);
				break;
			case 6:
				print_tree_prod(my_tree_agr);
				break;
			case 7:
				upload_tree_prod(my_tree_agr);
				break;
			default:
				printf("\nERROR IN CHANGE A FUNCTION\n");
				return 9;
		}
		return 0;
	}
	else
	{
		printf("please try again\n\n");
		return 1;
	}
}

