//sctruct for Node
struct node
{
	int key;
	struct node * left;
	struct node * right;
	struct node * next;
	char data[255];
};

//sctruct for tree
struct tree
{
	struct node * root;
};

//------------------------------support-functions------------------------------

//search and return Node with least key
struct node * tree_minimum(struct tree * search_tree)
{
	struct node * search_node;
	search_node = search_tree->root;
	while (search_node->left != NULL)
		search_node = search_node->left;
	return search_node;
}

//search and return Node with greatest key
struct node * tree_maximum(struct tree * search_tree)
{
	struct node * search_node;
	search_node = search_tree->root;
	while (search_node->right != NULL)
		search_node = search_node->right;
	return search_node;
}

//search Node with key, that largest among smaller
struct node * prev_node_f(struct tree * search_tree, int item)
{
	struct node * current;
	struct node * successor;
	current = search_tree->root;
	successor = NULL;
	while(current != NULL)
	{
		if ((int)current->key < item)
		{
			successor = current;
			current = current->right;
		}
		else
		{
			current = current->left;
		}
	}
return successor;
}

//search Node with key, that smallest among larger
//without using *next
struct node * next_node_f(struct tree * search_tree, int item)
{
	struct node * current;
	struct node * successor;
	current = search_tree->root;
	successor = NULL;
	while(current != NULL)
	{
		if ((int)current->key > item)
		{
			successor = current;
			current = current->left;
		}
		else
		{
			current = current->right;
		}
	}
return successor;
}

//-------------------------------print-functions-------------------------------

//print one Node
void print_node(const struct node * print_node)
{
	printf("%d\t-\t",print_node->key);
	fputs(print_node->data,stdout);
}

//print tree from any node
static void walk(const struct node * search_node, int deep)
{
	if(search_node == NULL) return;
	walk(search_node->left, deep+1);
	printf("Deep is %d\n",deep);
	print_node(search_node);
	walk(search_node->right, deep+1);
}	

//print tree from root (function is called with tree, not Node)
void walk2(const struct tree * my_tree)
{
	if (my_tree->root == NULL) 
	{
		printf("Tree is empty\n");
	}
	else
	{
		int deep = 0;
		walk(my_tree->root, deep);
	}
}

//print tree like a matrix (without recursion, but with using *next)
void print_matrix(struct tree * my_tree)
{
	if (my_tree->root == NULL)
	{
		printf("Tree is empty\n");
	}
	else 
	{
		struct node * current;
		current = tree_minimum(my_tree);
		while (current != NULL)
		{
			print_node(current);
			current = current->next;
		}
	}
}

//------------------------garbage-collection-functions-------------------------

//del tree from any node
static void destroy(struct node * search_node)
{
 	if(search_node == NULL) return;
 	destroy(search_node->left);
 	destroy(search_node->right);
 	free(search_node);
} 

//del tree form root (function is called with tree, not Node)
void bin_destroy(struct tree * search_tree)
{
	destroy(search_tree->root);
	free(search_tree);
}

//--------------------standart-binary-search-tree-functions--------------------

//create tree (without any nodes)
struct tree * tree_create(void)
{
	struct tree * new_tree = malloc(sizeof * new_tree);
	if (new_tree == NULL) return NULL;
	new_tree->root = NULL;
	return new_tree;
}

//binary search by key
struct node * bin_search(const struct tree * search_tree, int item)
{
	const struct node * search_node;
	search_node = search_tree->root;
	for(;;)
	{
		if (search_node == NULL) { return search_node; }
		else if (item == search_node->key) { return search_node; }
		else if (item > search_node->key) { search_node = search_node->right; } 
		else { search_node = search_node->left; } 
	}
}

//insert Node by key with data
int insert(struct tree * search_tree, char *info, int item)
{
	struct node * search_node, **new; 
	new = &search_tree->root;
	search_node = search_tree->root; 
	for(;;)
	{
		if(search_node == NULL)
		{
			search_node = *new = malloc(sizeof * search_node);
			if(search_node != NULL)
			{
				search_node->key = item;
				strcpy(search_node->data,info);
				search_node->left = search_node->right=NULL;
				//start insertion
				struct node * prev_node;
				prev_node = prev_node_f(search_tree, item);
				if (prev_node != NULL)
				{
					search_node->next = prev_node->next;
					prev_node->next = search_node;
				}
				else
				{
					struct node * next_node;
					next_node = next_node_f(search_tree, item);
					if (next_node == NULL) { search_node->next = NULL; }
					else { search_node->next = next_node; }
				}
				//finish insertion		
				return 0;
			}
			else return 9;
		}
		else if(item == search_node->key) return 1;
			else if(item > search_node->key)
				{
					new = &search_node->right;
					search_node = search_node->right;
				}
				else
				{
					new = &search_node->left;
					search_node = search_node->left;
				}
	}
}

//delete Node by key
int delete(struct tree * search_tree, int ** item)
{
	struct node ** q,*z;
	q = &search_tree->root;
	z = search_tree->root;
	//search node
	for(;;)
	{
		if(z == NULL) return 1;
		else if(item == (int **)z->key) break;
		else if(item > (int **)z->key)
		{
			q = &z->right;
			z = z->right;
		}
		else
		{
			q = &z->left;
			z = z->left;
		}
	}
	//make insertion
	struct node * prev_node;
	prev_node = prev_node_f(search_tree, (int)z->key);
	if (prev_node != NULL)
	{
		prev_node->next = z->next;
	}
	//del node
	if(z->right == NULL) 
	{
		*q = z->left;
	}
	else
	{
		struct node * y = z->right;
		if(y->left == NULL)
		{
			y->left = z->left;
			*q=y;
		}
		else
		{
			struct node * x=y->left;
			while(x->left != NULL)
			{
				y = x;
				x = y->left;
			}
			y->left = x->right;
			x->left = z->left;
			x->right = z->right;
			*q=x;
		}
	}
	free(z);
	return 0;
}

