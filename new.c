#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "mystruct.c" 
#include "myoperations.c" 

/*
all part of program that return int not data:
return 0 - no error, successfully 
return 1 - no error, not succesfully
return 9 - error (should not appear)

return (-1) - exit (only in one place - agregator_prod)
*/

void show_main_menu(void)
{
//---------------MAIN-MENU---------------
printf("What do you want to do?\n");
printf("-------main functions------\n");
printf("Press '1' for add a Node by key\n");
printf("Press '2' for del a Node by key\n");
printf("Press '3' for search a Node by key\n");
printf("Press '4' for print the table\n");
printf("Press '5' for search a Node with key, which is the smallest, but larger than the given\n");
printf("Press '0' for exit\n");
printf("-----another functions-----\n");
printf("Press '6' for print the table like a tree\n");	
printf("Press '7' for download the table from file\n");
}


int  main()
{
	//--------------PREPARATION--------------
	struct tree * my_tree = tree_create();
	int res;
	//---------DO-SOMETHING-WITH-TREE---------	
	int i = -1;
	int resp;
	while(1) {
		show_main_menu();
		//one unfixed bug
		//lines like '1adada' will be valid for the value '1' etc
		scanf("%d",&i);
		scanf("%*[^\n]");
		//function-agregator in myoperations.c
		resp = agregator_prod(i, my_tree); 
		if (resp == -1) return 0; //exit
		else if (resp == 0)	{i = -1;} //next operation
		else if (resp == 1) {i = -1;} //try again
		else if (resp == 9) return 0; //error
	}
	return 0 ;
}

